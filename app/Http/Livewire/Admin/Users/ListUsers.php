<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;
use Livewire\WithPagination;

class ListUsers extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';


    public $state = [];

    public $user;

    public $showEditModal = false;

    public $userBeingRemoved = null;

    public $searchTerm = null;


    protected $listeners = [
        'deleteConfirmed' => 'deleteUser'
    ];


    public function addNew()
    {
        $this->reset();

        $this->showEditModal = false;

        $this->dispatchBrowserEvent('show-form');
    }


    public function createUser()
    {

        $validatedData = Validator::make($this->state, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ])->validate();

        $validatedData['password'] = bcrypt($validatedData['password']);

        User::create($validatedData);

        $this->dispatchBrowserEvent('hide-form', ['message' => 'User added successfully']);
    }


    public function edit(User $user)
    {
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user;

        $this->state = $user->toArray();

        $this->dispatchBrowserEvent('show-form');
    }

    public function updateUser()
    {
        $validatedData = Validator::make($this->state, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->user->id,
            'password' => 'sometimes|confirmed',
        ])->validate();

        if (!empty($validatedData['password'])) {
            $validatedData['password'] = bcrypt($validatedData['password']);
        }

        $this->user->update($validatedData);

        $this->dispatchBrowserEvent('hide-form', ['message' => 'User updated successfully']);
    }

    public function confirmUserRemoval($userId)
    {
        $this->userBeingRemoved = $userId;

        $this->dispatchBrowserEvent('show-delete-confirmation');
    }

    public function deleteUser()
    {
        $user = User::findOrFail($this->userBeingRemoved);
        $user->delete();
        $this->dispatchBrowserEvent('deleted', ['message' => 'User deleted successfully']);
    }



    public function render()
    {

        $users = User::query()
            ->where('name', 'like', '%' . $this->searchTerm . '%')
            ->orWhere('email', 'like', '%' . $this->searchTerm . '%')
            ->latest()->paginate(10);
        return view('livewire.admin.users.list-users', compact('users'));
    }
}
